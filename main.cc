#include <iostream>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <string>
#include "processor.h"
#include "instruction.h"
#include "memory.h"
#include "alu.h"
#include "rom.h"
#include "io.h"

void print_array(int array[], int size){

    for(int i =0; i < size; i++)
        std::cout << array[i] ;
    std::cout << std::endl << std::endl;
}

int main(int argc, char * argv[]){

  IO io;
  io.boot_routine();
  //io.debug();

  ROM rom(argv[1]);

  Processor processor(&io);
  int pc[15] = {0};
  processor.get_pc(pc);

  int instruction[16] = {0};

  while(rom.fetch_instruction(pc, instruction)){
    processor.compute(instruction);
    processor.get_pc(pc);
   // io.actualize_keyboard();
   // processor.debug();
    io.refresh_frame();
  }


    return 0;
}
