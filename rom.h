#ifndef _ROM_H
#define _ROM_H

#include <iostream>
#include <cstdint>
#include <string>
#include <cstring>
#include <fstream>
#include <vector>

class ROM{
  private:
    std::vector<int> program;

  public:
    ROM(std::string path);
    bool fetch_instruction(int pc[], int instruction[]); //pc 15 bits
    void display_rom();

};


#endif
