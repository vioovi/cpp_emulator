#ifndef _INSTRU_H
#define _INSTRU_H

#include <iostream>

class Instruction
{
    private:
        int m_instruction[16];
        bool m_error;
        int m_a_or_c; // 0 means a, otherwise it's c instru


    public:
        Instruction();
        void set_instruction(int instruction[16]);
        bool get_error();
        int a_or_m();
        void get_a_value(int a_value[15]);
        void get_c_comp(int c_comp[7]);
        void get_c_dest(int c_dest[3]);
        void get_c_jump(int c_jump[3]);
        void display_a_or_m();

};

#endif
