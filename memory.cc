
#include "memory.h"

void display_dec(int* array){
    int address_dec = 0;
    int coeff = 1;
    for (int i = 15; i>=0; i--){
        address_dec += (array[i] * coeff);
        coeff *= 2;
    }
    std::cout << address_dec;
}


Memory::Memory(IO* io_p){
    io = io_p;
    for (int i =0; i < (16384*16); i++)
        ram[i] = 0;
}

bool Memory::read(int address[16], int out[16]){
    int address_dec = 0;
    int coeff = 1;
    for (int i = 15; i>=0; i--){
        address_dec += (address[i] * coeff);
        coeff *= 2;
    }

    //std::cout <<  "read: ";
    //display_dec(out);
    //std:: cout << " in "<<address_dec << std::endl;

    if (address_dec < 0x4000){
        address_dec *= 16;

        for (int i = 0; i< 16; i++)
            out[i] = ram[i+address_dec];

        return true;
    } else if (address_dec >= 0x4000 && address_dec < 0x6000){
        io->read_pixel(address_dec, out);
        return true;
    } else if (address_dec == 0x6000){
       io->read_keyboard(out);
       //for (int i =0; i<16; i++)
        //   out[i] = 0;

    } else {
        return false;
    }
}

bool Memory::write(int address[16], int data[16]){
    int address_dec = 0;
    int coeff = 1;
    for (int i = 15; i>=0; i--){
        address_dec += (address[i] * coeff);
        coeff *= 2;
    }
   // std::cout <<  "write: ";
   // display_dec(data);
   // std:: cout << " in "<<address_dec << std::endl;
    if (address_dec < 0x4000){
        address_dec *= 16;

        for (int i = 0; i< 16; i++)
            ram[i+address_dec] = data[i];

        return true;
    }
    else if (address_dec >= 0x4000 && address_dec <0x6000) {
        io->write_pixel(address_dec, data);
        return true;
    } else {
        return false;
    }
}

void Memory::debug(int max){
    max *= 16;

    for (int i = 0; i< max; i+=16){
        std::cout << (i/16) << ": " ;
        for (int j =i ; j < (16+i); j++)
            std::cout << ram[j];
        std::cout << std::endl;
    }


}
