#ifndef _MEMORY_H
#define _MEMORY_H

#include <iostream>
#include "io.h"

class Memory{
    private:
        int ram[16384*16];
        IO* io;

        void screen_map_read();
        void screen_map_write();
        void keyboard_map_read();

    public:
        Memory(IO* io_p);
        bool read(int address[16], int out[16]);
        bool write(int address[16], int data[16]);
        void debug(int max);


};

void display_dec(int* array);


#endif
