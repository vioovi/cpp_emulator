#ifndef _IO_H
#define _IO_H

#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

typedef unsigned short Event;

class IO{
    private:
        sf::RenderWindow *window;
        sf::Image frame_buffer;
        sf::Texture texture;
        sf::Sprite sprite;
        Event event;
        bool upload;


    public:
        IO();
        void boot_routine();
        void actualize_keyboard();
        void refresh_frame();
        void debug();
        void read_pixel(int address, int output[16]);
        void write_pixel(int address, int value[16]);
        void read_keyboard(int output[16]);
        ~IO();


};

#endif
