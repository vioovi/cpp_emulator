#include "processor.h"

Processor::Processor(IO* io_p){
    io = io_p;
    instruction = new Instruction();
    alu = new ALU(m_D_register, m_A_register, m_M_register);
    memory = new Memory(io);

    for (int i =0; i < 16; i++){
        m_A_register[i] = 0;
        m_D_register[i] = 0;
        m_M_register[i] = 0;
    }
    for (int i =0; i<15;i++)
      m_PC[i] = 0;

}

void Processor::compute(int instruction_p[16]){
    instruction->set_instruction(instruction_p);
    if (instruction->a_or_m()){
        // c instruction
        memory->read(m_A_register, m_M_register);
        //alu->update_M(m_M_register);
        int output[16] = {0};
        int comp[7] = {0};

        // calculate ALU output
        instruction->get_c_comp(comp);
        alu->set_comp(comp);
        alu->compute(output);

        // store output
        int store[3] = {0};
        instruction->get_c_dest(store);
        if (store[2])
            memory->write(m_A_register, output);
        if (store[0])
            memcpy(m_A_register, output, sizeof(int)*16);
        if (store[1])
            memcpy(m_D_register, output, sizeof(int)*16);

        // pc
        int sign = range(output);
        int jump[3] = {0};
        instruction->get_c_jump(jump);
        if (sign == 0 && jump[0])
          memcpy(m_PC, &m_A_register[1], sizeof(int)*15);
        else if (sign ==1 && jump[1])
          memcpy(m_PC, &m_A_register[1], sizeof(int)*15);
        else if (sign ==2 && jump[2])
          memcpy(m_PC, &m_A_register[1], sizeof(int)*15);
        else
          inc_PC();

    } else {
        // a instruction
        instruction->get_a_value(&m_A_register[1]);
        m_A_register[0] = 0;
        inc_PC();
    }
   /* std::cout << "pc: " ;
    int integ = 0;
    int coef =1;
    for (int i=14; i>=0; i--){
        integ += (m_PC[i] * coef);
        coef *= 2;
    }
    std::cout << integ<<std::endl;
*/

}

int Processor::range(int* output){
  if (output[0] ==  1){
    return 0;
  }
  else{
    int max = output[0];
    for (int i = 0; i<16;i++){
      if (max < output[i])
        max = output[i];
    }
    if (max)
      return 2;
    else {
      return 1;
    }
  }
}

void Processor::get_pc(int pc_p[15]){
  memcpy(pc_p, m_PC, 15*sizeof(int));
}

void Processor::inc_PC(){
    int b[15] = {0};
    b[14] = 1;
    int carry = 0;
    for (int i = 14; i >=0 ; i--){
        int sum = carry + b[i] + m_PC[i];
        m_PC[i] = sum%2;
        carry = sum/2;
    }

}

void Processor::debug(){

    std::cout << "DEBUG REGISTERS" <<std::endl;
    std::cout << "a :";
    for (int i = 0; i< 16; i++)
        std::cout << m_A_register[i] ;
    std::cout << std::endl << "d :";
    for (int i = 0; i< 16; i++)
        std::cout << m_D_register[i];
    std::cout << std::endl << "pc :";
    for (int i = 0; i< 15; i++)
        std::cout << m_PC[i];
    std::cout << std::endl << std::endl;

    std::cout << "DEBUG MEMORY" <<std::endl;
    memory->debug(20);

}

Processor::~Processor(){
    delete instruction;
    delete alu;
    delete memory;

}
