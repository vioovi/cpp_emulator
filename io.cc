#include "io.h"

IO::IO(){
    std::string vioovi = "VIOOVI Is Our Own Emulator";
    window = new sf::RenderWindow(sf::VideoMode(511,255), vioovi);
    frame_buffer.create(511,255,sf::Color::Black);
    texture.loadFromImage(frame_buffer);
    sprite.setTexture(texture);

}

void IO::boot_routine(){
    sf::Font font;
    if(font.loadFromFile("lucida-console.ttf")){

        sf::Text text;
        text.setFont(font);
        text.setString(" > VIOOVI is booting...");
        text.setCharacterSize(12);
        text.setColor(sf::Color::White);

        for (int i =0; i< 120; i++){

            window->clear();
            window->draw(text);
            window->display();
        }
        window->clear(sf::Color::Black);
        window->display();
    } else {
        std::cout << "police error" <<std::endl;
    }
}
void IO::debug (){

    frame_buffer.setPixel(50,50,sf::Color::White);
    texture.update(frame_buffer);
}

void IO::refresh_frame(){
    if (upload){
        window->clear();
        window->draw(sprite);
        window->display();
        upload = false;
    }
}

void IO::read_pixel(int address, int output[16]){
    address -= 0x4000;
    int row = address /32;
    int word = address %32;

    for (int i =0; i <16; i++){
        if(frame_buffer.getPixel(word*16+i,row) == sf::Color::Black)
            output[15-i] = 0;
        else
            output[15-i] = 1;
    }
    texture.update(frame_buffer);
}

void IO::actualize_keyboard(){
    event =0;
    for (int i =0; i< 26;i++){
        sf::Keyboard::Key key = static_cast<sf::Keyboard::Key>(i);
        if (sf::Keyboard::isKeyPressed(key))
            event = i + 0x61;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        event = 130;
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        event = 132;
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        event = 131;
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        event = 133;
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
        event = 140;
}

void IO::read_keyboard(int output[16]){
    Event div = event;
    short reste = 0;
    int i = 15;
    while (div >= 0){
        output[i] = div % 2;
        div = div /2;
      //  std::cout << std::endl << output[i];
        --i;
    }
   // std::cout <<std::endl;

}

void IO::write_pixel(int address, int value[16]){
   // std::cout << "g: "<<address << std::endl;
    address -= 0x4000;
    int row = address /32;
    int word = address %32;

    for (int i =0; i <16; i++){
        if (value[15-i])
            frame_buffer.setPixel(word*16+i, row, sf::Color::White);
        else
            frame_buffer.setPixel(word*16+i, row, sf::Color::Black);
    }
    texture.update(frame_buffer);
    upload = true;
}


IO::~IO(){
    delete window;

}
