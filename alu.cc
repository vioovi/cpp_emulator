#include <iostream>
#include "alu.h"

ALU::ALU(int register_d[16], int register_a[16], int register_m[16])
{
    m_D = register_d;
    m_A = register_a;
    m_M = register_m;
    for (int i = 0; i<16;i++){
        zero_vector[i] = 0;
        minus_one_vector[i] = 1;
        one_vector[i] = 0;
    }
    one_vector[15] = 1;
}

int ALU::set_comp(int comp[7]){
    m_comp = 0x00;
    m_am = comp[0];
    int coeff = 1;

    for (int i = 6; i > 0; i--){
        m_comp += comp[i]*coeff;
        coeff *= 2;
    }
}

void ALU::display_comp(){
    std::cout<<"instruction: ";
    std::cout<<std::hex<<int(m_comp)<<std::endl;
}

void ALU::update_M(int m[16]){
    // to do
    for (int i = 0; i<16; i++)
        m_M[i] = m[i];
}



/*
 ********* ALU functions ******************
 */

void ALU::add(int a[16], int b[16], int result[16]){
    int remainder = 0;
    for(int i = 15; i >=0; i--){
        result[i] = (a[i] + b[i] + remainder)%2;
        if ((a[i] + b[i] + remainder)>1){
            remainder = 1;
        }else{
            remainder = 0;
        }
    }
}

void ALU::sub(int a[16], int b[16], int result[16]){
    int remainder = 0;
    for(int i = 15; i >=0; i--){
        if(a[i] >= (b[i]+remainder)){
            result[i] = a[i] - (b[i]+remainder);
            remainder = 0;
        } else{
            result[i] = (0x02+a[i]) - (b[i] + remainder);
            remainder = 1;
        }
    }

}

void ALU::_and(int a[16], int b[16], int result[16]){
    for (int i = 0; i<16; i++)
        result[i] = a[i] & b[i];
}

void ALU::_or(int a[16], int b[16], int result[16]){
    for (int i = 0; i<16; i++)
        result[i] = a[i] | b[i];
}

void ALU::_not(int a[16], int result[16]){
    for (int i = 0; i < 16; i++){
        if(a[i] == 0x00)
            result[i] = 0x01;
        else
            result[i] = 0x00;
    }
}

/*
 ********** Global computation *************
 */

void ALU::compute(int result[16]){
    switch (m_comp) {
        case 0x2a:{
            std::memcpy(result, zero_vector, sizeof(int)*16);
        } break;
        case 0x3f:{
            std::memcpy(result, one_vector, sizeof(int)*16);
        } break;
        case 0x3a:{
            std::memcpy(result, minus_one_vector, sizeof(int)*16);
        } break;
        case 0x0c:{
            std::memcpy(result, m_D, sizeof(int)*16);
        }break;
        case 0x30:{
            if (m_am == 0x00){
                std::memcpy(result, m_A, sizeof(int)*16);
            } else if (m_am == 0x01){
                std::memcpy(result, m_M, sizeof(int)*16);
            } else{
                std::cout << "error format" << std::endl;
            }
        } break;
        case 0x0d:{
            _not(m_D, result);
        }break;
        case 0x31:{
            if (m_am == 0x00){
                _not(m_A, result);
            } else if (m_am == 0x01){
                _not(m_M, result);
            } else{
                std::cout << "error format" << std::endl;
            }
        } break;
        case 0x0f:{
            sub(zero_vector, m_D, result);
        }break;
        case 0x33:{
            if (m_am == 0x00){
                sub(zero_vector, m_A, result);
            }
            else if (m_am == 0x01){
                sub(zero_vector, m_M, result);
            }
            else{
                std::cout << "error format" << std::endl;
            }
        }break;
        case 0x1f:{
            add(one_vector, m_D, result);
        }break;
        case 0x37:{
            if (m_am == 0x00){
                add(one_vector, m_A, result);
            }
            else if (m_am == 0x01){
                add(one_vector, m_M, result);
            }
            else{
                std::cout << "error format" << std::endl;
            }
        }break;
        case 0x0e:{
            sub(m_D, one_vector, result);
        } break;
        case 0x32:{
            if (m_am == 0x00){
                sub(m_A, one_vector, result);
            }
            else if (m_am == 0x01){
                sub(m_M, one_vector, result);
            }
            else{
                std::cout << "error format" << std::endl;
            }
        } break;
        case 0x02:{
            if (m_am == 0x00){
                add(m_D, m_A, result);
            }
            else if (m_am == 0x01){
                add(m_D, m_M, result);
            }
            else{
                std::cout << "error format" << std::endl;
            }
        }break;
        case 0x13:{
            if (m_am == 0x00){

                sub(m_D, m_A, result);
            }
            else if (m_am == 0x01){
                sub(m_D, m_M, result);
            }
            else{
                std::cout << "error format" << std::endl;
            }
        } break;
        case 0x07:{
            if (m_am == 0x00){
                sub(m_A, m_D, result);
            }
            else if (m_am == 0x01){
                sub(m_M, m_D, result);
            }
            else{
                std::cout << "error format" << std::endl;
            }
        } break;
        case 0x00:{
            if (m_am == 0x00){
                _and(m_D, m_A, result);
            }
            else if (m_am == 0x01){
                _and(m_D, m_M, result);
            }
            else{
                std::cout << "error format" << std::endl;
            }
        } break;
        case 0x15:{
            if (m_am == 0x00){
                _or(m_D, m_A, result);
            }
            else if (m_am == 0x01){
                _or(m_D, m_M, result);
            }
            else{
                std::cout << "error format" << std::endl;
            }
        } break;
        default:
            std::cout << "error alu decoding" << std::endl;
        break;
    }
}
