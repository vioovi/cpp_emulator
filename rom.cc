#include <iostream>
#include "rom.h"

using namespace std;

ROM::ROM(string path){
    ifstream file(path.c_str(), ios::in);

    if (!file)
        cerr << "fail file"  << endl;


    string line;

    while (getline(file, line)){
        for (int i= 0; i < line.length();i++){
            if (line[i] == '0')
                program.push_back(0);
            else
                program.push_back(1);
        }
    }

    if (file)
        file.close();
}

bool ROM::fetch_instruction(int pc[], int instruction[]){
    int pc_dec = 0;
    int coeff = 1;
    for (int i =14; i >=0; i--){
        pc_dec += (coeff*pc[i]);
        coeff *= 2;
    }

    pc_dec *= 16;
    if ((pc_dec +16)>program.size()){
        for (int i = 0; i<16; i++)
            instruction[i] = -1;
        return false;
    } else {
        for (int i = 0; i<16; i++ )
            instruction[i] = program[i+pc_dec];

        return true;
    }
}


void ROM::display_rom(){

    for (int i = 0; i < program.size(); i+=16){
        for (int j = i; j< (i+16); j++)
            cout << program[j];
        cout << endl;
   }

}


