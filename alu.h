#ifndef _ALU_H
#define _ALU_H

#include <iostream>
#include <cstdint>
#include <string>
#include <cstring>

class ALU
{
    private:
        int* m_D;
        int* m_A;
        int* m_M;
        uint8_t m_comp;
        int m_am;

        /* Special vectors */
        int zero_vector[16];
        int minus_one_vector[16];
        int one_vector[16];

        /* basic operations */

        void add(int a[16], int b[16], int result[16]);
        void sub(int a[16], int b[16], int result[16]);
        void _or(int a[16], int b[16], int result[16]);
        void _and(int a[16], int b[16], int result[16]);
        void _not(int a[16], int result[16]);


    public:
        ALU(int register_d[16], int register_a[16], int register_m[16]);
        void update_M(int m[16]);
        int set_comp(int comp[7]);
        void display_comp();
        int a_or_m(); // 0 means a, otherwise it's m

        void compute(int result[16]);
};

#endif
