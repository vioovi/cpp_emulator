#include <iostream>
#include "instruction.h"


Instruction::Instruction()
{
    m_a_or_c = 0;
}

void Instruction::set_instruction(int instruction[16]){
    m_a_or_c = instruction[0];
    for (int i = 0; i < 16; i++){
        m_instruction[i] = instruction[i];
        if (m_instruction[i] != 0 &&
                m_instruction[i] != 1){
            m_error = true;
        }
    }
}

bool Instruction::get_error(){
    return m_error;
}

int Instruction::a_or_m(){
    return m_a_or_c;
}

void Instruction::get_a_value(int a_value[15]){
    for(int i = 0; i<15; i++)
        a_value[i] = m_instruction[i+1];
}

void Instruction::get_c_comp(int c_comp[7]){
    for(int i = 0; i<7; i++)
        c_comp[i] = m_instruction[i+3];
}

void Instruction::get_c_dest(int c_dest[3]){
    for(int i = 0; i<3; i++)
        c_dest[i] = m_instruction[i+10];
}

void Instruction::get_c_jump(int c_jump[3]){
    for(int i = 0; i<3; i++)
        c_jump[i] = m_instruction[i+13];
}
void Instruction::display_a_or_m(){
    std::cout<<m_a_or_c<<std::endl;
}
