#ifndef _PROCESSOR_H
#define _PROCESSOR_H

#include <iostream>
#include "instruction.h"
#include "alu.h"
#include "memory.h"
#include <cstring>
#include "io.h"


class Processor{
    private:
        IO* io;
        //registers
        int m_A_register[16];
        int m_D_register[16];
        int m_PC[15];
        // logicaal register
        int m_M_register[16];
        // dynamic components
        Instruction* instruction;
        ALU* alu;
        Memory* memory;
        //increment pc
        void inc_PC();
        // return 0 if <0
        //      1, == 0
        //      2 > 0
        int range(int* output);

    public:
        Processor(IO* io_p);
        void compute(int instruction_p[16]);
        void get_pc(int pc_p[15]);
        void debug ();
        ~Processor();



};


#endif
